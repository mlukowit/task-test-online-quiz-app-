# Task Test online Quiz App 
A Web-based online quiz application that displays data from a MySQL database.

Uses a Bootstrap Time Jscript to display time from server time on each page and process meter.
Results are tallied at end of tests.  

A database connection must be made a MySQL database.  A DB exception handling connection script is in directory inc. to help with db connections.

Test processes including top header results on numbers of wrong or right with a checkbox form to specify test category.